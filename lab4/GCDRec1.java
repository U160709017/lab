public class GCDRec1 {

	public static void main(String[] args) {
		int n1 = Integer.parseInt(args[0]);
		int n2 = Integer.parseInt(args[1]);
		int hcf = hcf(n1, n2);

		System.out.println(hcf);
	}

	public static int hcf(int n1, int n2) {
		if (n2 != 0)
			return hcf(n2, n1 % n2);
		else
			return n1;
	}
}
