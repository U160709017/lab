package lab;

public class Main {

	public static void main(String[] args) {
		Rectangle Rectangle = new Rectangle();
		int a = 5;
		int b = 6;
		Rectangle.sideA = a;
		Rectangle.sideB = b;
		System.out.println("The area of the rectangle is " + lab.Rectangle.area(a, b) + ".");
		System.out.println("The perimeter of the rectangle is " + lab.Rectangle.perimeter(a, b) + ".");
		
		
		Circle Circle = new Circle();
		int c = 10;
		Circle.radius = c;
		System.out.println("If pi is equal 3, the area of the circle is " + lab.Circle.area(c) + ".");
		System.out.println("If pi is equal 3, the perimeter of the circle is " + lab.Circle.perimeter(c) + ".");
	}

}
